# Create-react-app with AWS Amplify Auth and Storage

This simple app implements withAuthenticator HOC to provide a basic authentication flow for signing up/signing in users as well as protected client side routing using AWS Amplify. 

It then allows authenticated user to list items in S3 bucket and also upload item to the bucket.

Auth features: User sign up, User sign in, Multi-factor Authentication, User sign-out.

Storage features: List item, put item

[View Demo](https://master.d2hvwgkw24or21.amplifyapp.com/)

![Amplify Auth](src/images/auth.gif)

## AWS Amplify Hosting

This webpage (https://master.d2hvwgkw24or21.amplifyapp.com/) is currently hosted on AWS Amplify

## Run locally with the Amplify CLI

  Clone the repo that was just forked in your account

  ```
  git clone git@github.com:<username>/create-react-app-auth-amplify.git
  ```

  ```
  cd create-react-app-auth-amplify && npm install
  ```

  ```
  npm start
  ```  

## Deploy to S3 bucket hosting

  Install Amplify CLI

  ```
  npm install -g @aws-amplify/cli
  ```

  Run Amplify command to deploy to S3

  ```
  amplify publish
  ```
  
  This command will build the project and upload the package to S3 buckets defined in the aws-exports.js
  
  You can browse to the hosted webpage using the aws_content_delivery_url. In this case it is this [link](http://authcra-20190924171637-hostingbucket-dev.s3-website.ap-northeast-2.amazonaws.com)