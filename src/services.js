import aws_exports from './aws-exports';
import Amplify, { Storage } from 'aws-amplify';
// Amplify.configure(aws_exports);

export function configureAmplify() {
    Amplify.configure(
    {
     Auth: {
       identityPoolId: aws_exports.aws_cognito_identity_pool_id,
       region: aws_exports.aws_cognito_region,
       userPoolId: aws_exports.aws_user_pools_id,
       userPoolWebClientId: aws_exports.aws_user_pools_web_client_id,
      },
    Storage: { 
       bucket: aws_exports.aws_user_files_s3_bucket,
       region: aws_exports.aws_user_files_s3_bucket_region,
       identityPoolId: aws_exports.aws_cognito_identity_pool_id
      }
    }
   );
  }
  //Configure Storage with S3 bucket information
  export function SetS3Config(bucket, level){
     Storage.configure({ 
            bucket: bucket,
            level: level,
            region: aws_exports.aws_user_files_s3_bucket_region,  
            identityPoolId: aws_exports.aws_cognito_identity_pool_id
         });
  }