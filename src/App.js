import React, { Component } from 'react';
import './App.css';
import { css } from 'glamor';
import { withAuthenticator } from 'aws-amplify-react';
import { SetS3Config } from "./services";
import Amplify, { Storage } from 'aws-amplify';
import awsconfig from './aws-exports';
import ItemList from './components/ItemList';

Amplify.configure(awsconfig);

class App extends Component {
  state = {
    imageName: "",
    imageFile: "",
    response: "",
    firmwares: []
  };

  componentDidMount() {
    this.listsOfFile();
  }

  listsOfFile = () => {
    Storage.list('552/')
    .then(result => 
      this.setState({ firmwares : result })
    )    
    .catch(err => console.log(err));
  };

  uploadFile = () => {
    if (this.state.imageName === ''){
      this.setState({response: "No file uploaded."});
      return;
    }

    SetS3Config("authcra-otafirmware-dev", "public");
    Storage.put(`552/${this.upload.files[0].name}`,
                this.upload.files[0],
                { contentType: this.upload.files[0].type })
      .then(result => {
        this.upload = null;
        this.setState({ response: "Success uploaded file!" });
        this.listsOfFile();
      })
      .catch(err => {
        this.setState({ response: `Cannot uploaded file: ${err}` });
      });
  };

  render() {
    let { firmwares } = this.state

    return (
      <div {...css(styles.container)}>    
        <p {...css(styles.title)}>S3 Upload Example</p>
        <div className="App">
          <input
            type="file"
            accept=".bin"
            style={{ display: "none" }}
            ref={ref => (this.upload = ref)}
            onChange={e =>
              this.setState({
                imageFile: this.upload.files[0],
                imageName: this.upload.files[0].name
              })
            }
          />
          <input value={this.state.imageName} placeholder="Select file" />
          <button
            onClick={e => {
              this.upload.value = null;
              this.upload.click();
            }}
            loading={this.state.uploading}
          >
            Browse
          </button>

          <button onClick={this.uploadFile}> Upload File </button>

          {!!this.state.response && <div>{this.state.response}</div>}
        </div>

        <p {...css(styles.title)}>Uploaded Firmware</p>
        <ItemList 
          firmwares={firmwares}
        />
      </div>
    );
  }
}

const styles = {
  container: {
    width: 360,
    margin: '0 auto',
    borderBottom: '1px solid #ededed',
  },
  form: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  input: {
    height: 35,
    width: '360px',
    border: 'none',
    outline: 'none',
    marginLeft: 10,
    fontSize: 20,
    padding: 8,
  }
}

export default withAuthenticator(App, true);
