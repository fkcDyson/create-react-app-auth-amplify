import React from 'react';
import { css } from 'glamor';

class ItemList extends React.Component {
    render() {
        return (
            <div {...css(styles.container)}>
                    <ul>
                        {this.props.firmwares.map(item => (
                            <li key={item.key}>
                                <div>Name: {item.key} - Size: {item.size}</div>
                            </li>
                        ))}
                    </ul>
            </div>
        )
    }
}

const styles = {
    container: {
        width: 360,
        margin: '0 auto',
        borderBottom: '1px solid #ededed',
    },
    form: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        height: 35,
        width: '360px',
        border: 'none',
        outline: 'none',
        marginLeft: 10,
        fontSize: 20,
        padding: 8,
    }
}

export default ItemList